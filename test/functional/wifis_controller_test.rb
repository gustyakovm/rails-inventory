require 'test_helper'

class WifisControllerTest < ActionController::TestCase
  setup do
    @wifi = wifis(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:wifis)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create wifi" do
    assert_difference('Wifi.count') do
      post :create, wifi: { devise_own_id: @wifi.devise_own_id, devise_type_id: @wifi.devise_type_id, mac: @wifi.mac, model: @wifi.model, username: @wifi.username }
    end

    assert_redirected_to wifi_path(assigns(:wifi))
  end

  test "should show wifi" do
    get :show, id: @wifi
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @wifi
    assert_response :success
  end

  test "should update wifi" do
    put :update, id: @wifi, wifi: { devise_own_id: @wifi.devise_own_id, devise_type_id: @wifi.devise_type_id, mac: @wifi.mac, model: @wifi.model, username: @wifi.username }
    assert_redirected_to wifi_path(assigns(:wifi))
  end

  test "should destroy wifi" do
    assert_difference('Wifi.count', -1) do
      delete :destroy, id: @wifi
    end

    assert_redirected_to wifis_path
  end
end
