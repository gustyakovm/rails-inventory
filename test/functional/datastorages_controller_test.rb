require 'test_helper'

class DatastoragesControllerTest < ActionController::TestCase
  setup do
    @datastorage = datastorages(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:datastorages)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create datastorage" do
    assert_difference('Datastorage.count') do
      post :create, datastorage: { disks: @datastorage.disks, extenders: @datastorage.extenders, inventory_num: @datastorage.inventory_num, name: @datastorage.name, rackspace_id: @datastorage.rackspace_id, status_id: @datastorage.status_id, zone_id: @datastorage.zone_id }
    end

    assert_redirected_to datastorage_path(assigns(:datastorage))
  end

  test "should show datastorage" do
    get :show, id: @datastorage
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @datastorage
    assert_response :success
  end

  test "should update datastorage" do
    put :update, id: @datastorage, datastorage: { disks: @datastorage.disks, extenders: @datastorage.extenders, inventory_num: @datastorage.inventory_num, name: @datastorage.name, rackspace_id: @datastorage.rackspace_id, status_id: @datastorage.status_id, zone_id: @datastorage.zone_id }
    assert_redirected_to datastorage_path(assigns(:datastorage))
  end

  test "should destroy datastorage" do
    assert_difference('Datastorage.count', -1) do
      delete :destroy, id: @datastorage
    end

    assert_redirected_to datastorages_path
  end
end
