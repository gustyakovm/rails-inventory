require 'test_helper'

class ReplacePartsControllerTest < ActionController::TestCase
  setup do
    @replace_part = replace_parts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:replace_parts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create replace_part" do
    assert_difference('ReplacePart.count') do
      post :create, replace_part: { date_getting: @replace_part.date_getting, date_sending: @replace_part.date_sending, descr_getting: @replace_part.descr_getting, descr_sending: @replace_part.descr_sending, detail_type: @replace_part.detail_type, network_name: @replace_part.network_name }
    end

    assert_redirected_to replace_part_path(assigns(:replace_part))
  end

  test "should show replace_part" do
    get :show, id: @replace_part
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @replace_part
    assert_response :success
  end

  test "should update replace_part" do
    put :update, id: @replace_part, replace_part: { date_getting: @replace_part.date_getting, date_sending: @replace_part.date_sending, descr_getting: @replace_part.descr_getting, descr_sending: @replace_part.descr_sending, detail_type: @replace_part.detail_type, network_name: @replace_part.network_name }
    assert_redirected_to replace_part_path(assigns(:replace_part))
  end

  test "should destroy replace_part" do
    assert_difference('ReplacePart.count', -1) do
      delete :destroy, id: @replace_part
    end

    assert_redirected_to replace_parts_path
  end
end
