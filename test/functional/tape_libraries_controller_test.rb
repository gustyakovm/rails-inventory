require 'test_helper'

class TapeLibrariesControllerTest < ActionController::TestCase
  setup do
    @tape_library = tape_libraries(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tape_libraries)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tape_library" do
    assert_difference('TapeLibrary.count') do
      post :create, tape_library: { ip: @tape_library.ip, model_id: @tape_library.model_id, name: @tape_library.name, rackspace_id: @tape_library.rackspace_id, zone_id: @tape_library.zone_id }
    end

    assert_redirected_to tape_library_path(assigns(:tape_library))
  end

  test "should show tape_library" do
    get :show, id: @tape_library
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tape_library
    assert_response :success
  end

  test "should update tape_library" do
    put :update, id: @tape_library, tape_library: { ip: @tape_library.ip, model_id: @tape_library.model_id, name: @tape_library.name, rackspace_id: @tape_library.rackspace_id, zone_id: @tape_library.zone_id }
    assert_redirected_to tape_library_path(assigns(:tape_library))
  end

  test "should destroy tape_library" do
    assert_difference('TapeLibrary.count', -1) do
      delete :destroy, id: @tape_library
    end

    assert_redirected_to tape_libraries_path
  end
end
