require 'test_helper'

class DcatsControllerTest < ActionController::TestCase
  setup do
    @dcat = dcats(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dcats)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dcat" do
    assert_difference('Dcat.count') do
      post :create, dcat: { alias: @dcat.alias, name: @dcat.name }
    end

    assert_redirected_to dcat_path(assigns(:dcat))
  end

  test "should show dcat" do
    get :show, id: @dcat
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dcat
    assert_response :success
  end

  test "should update dcat" do
    put :update, id: @dcat, dcat: { alias: @dcat.alias, name: @dcat.name }
    assert_redirected_to dcat_path(assigns(:dcat))
  end

  test "should destroy dcat" do
    assert_difference('Dcat.count', -1) do
      delete :destroy, id: @dcat
    end

    assert_redirected_to dcats_path
  end
end
