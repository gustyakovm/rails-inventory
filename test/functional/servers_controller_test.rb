require 'test_helper'

class ServersControllerTest < ActionController::TestCase
  setup do
    @server = servers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:servers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create server" do
    assert_difference('Server.count') do
      post :create, server: { description: @server.description, hardware: @server.hardware, inventory_num: @server.inventory_num, ip: @server.ip, mac: @server.mac, model_details: @server.model_details, model_id: @server.model_id, name: @server.name, os_details: @server.os_details, os_id: @server.os_id, rackspace_id: @server.rackspace_id, rating: @server.rating, serial_num: @server.serial_num, software: @server.software, status_id: @server.status_id, zone_id: @server.zone_id }
    end

    assert_redirected_to server_path(assigns(:server))
  end

  test "should show server" do
    get :show, id: @server
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @server
    assert_response :success
  end

  test "should update server" do
    put :update, id: @server, server: { description: @server.description, hardware: @server.hardware, inventory_num: @server.inventory_num, ip: @server.ip, mac: @server.mac, model_details: @server.model_details, model_id: @server.model_id, name: @server.name, os_details: @server.os_details, os_id: @server.os_id, rackspace_id: @server.rackspace_id, rating: @server.rating, serial_num: @server.serial_num, software: @server.software, status_id: @server.status_id, zone_id: @server.zone_id }
    assert_redirected_to server_path(assigns(:server))
  end

  test "should destroy server" do
    assert_difference('Server.count', -1) do
      delete :destroy, id: @server
    end

    assert_redirected_to servers_path
  end
end
