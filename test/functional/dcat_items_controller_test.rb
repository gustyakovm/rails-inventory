require 'test_helper'

class DcatItemsControllerTest < ActionController::TestCase
  setup do
    @dcat_item = dcat_items(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dcat_items)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dcat_item" do
    assert_difference('DcatItem.count') do
      post :create, dcat_item: { cat_id: @dcat_item.cat_id, name: @dcat_item.name }
    end

    assert_redirected_to dcat_item_path(assigns(:dcat_item))
  end

  test "should show dcat_item" do
    get :show, id: @dcat_item
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dcat_item
    assert_response :success
  end

  test "should update dcat_item" do
    put :update, id: @dcat_item, dcat_item: { cat_id: @dcat_item.cat_id, name: @dcat_item.name }
    assert_redirected_to dcat_item_path(assigns(:dcat_item))
  end

  test "should destroy dcat_item" do
    assert_difference('DcatItem.count', -1) do
      delete :destroy, id: @dcat_item
    end

    assert_redirected_to dcat_items_path
  end
end
