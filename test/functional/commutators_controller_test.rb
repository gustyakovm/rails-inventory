require 'test_helper'

class CommutatorsControllerTest < ActionController::TestCase
  setup do
    @commutator = commutators(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:commutators)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create commutator" do
    assert_difference('Commutator.count') do
      post :create, commutator: { inventory_num: @commutator.inventory_num, model_id: @commutator.model_id, name: @commutator.name, serial_num: @commutator.serial_num }
    end

    assert_redirected_to commutator_path(assigns(:commutator))
  end

  test "should show commutator" do
    get :show, id: @commutator
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @commutator
    assert_response :success
  end

  test "should update commutator" do
    put :update, id: @commutator, commutator: { inventory_num: @commutator.inventory_num, model_id: @commutator.model_id, name: @commutator.name, serial_num: @commutator.serial_num }
    assert_redirected_to commutator_path(assigns(:commutator))
  end

  test "should destroy commutator" do
    assert_difference('Commutator.count', -1) do
      delete :destroy, id: @commutator
    end

    assert_redirected_to commutators_path
  end
end
