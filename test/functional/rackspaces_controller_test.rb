require 'test_helper'

class RackspacesControllerTest < ActionController::TestCase
  setup do
    @rackspace = rackspaces(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:rackspaces)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create rackspace" do
    assert_difference('Rackspace.count') do
      post :create, rackspace: { area_id: @rackspace.area_id, name: @rackspace.name }
    end

    assert_redirected_to rackspace_path(assigns(:rackspace))
  end

  test "should show rackspace" do
    get :show, id: @rackspace
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @rackspace
    assert_response :success
  end

  test "should update rackspace" do
    put :update, id: @rackspace, rackspace: { area_id: @rackspace.area_id, name: @rackspace.name }
    assert_redirected_to rackspace_path(assigns(:rackspace))
  end

  test "should destroy rackspace" do
    assert_difference('Rackspace.count', -1) do
      delete :destroy, id: @rackspace
    end

    assert_redirected_to rackspaces_path
  end
end
