class InfosController < ApplicationController
  

# GET /blades
  # GET /blades.json
  before_filter :set_category, :only => [:new, :edit, :create, :update, :index]
  before_filter :set_page_title
  before_filter :check_admin_permissions

  def set_page_title
    page_title(t(:infos), "icon-book")
  end

  def set_category
    @categories = DcatItem.get_by_alias("info_cats")
  end

  def check_admin_permissions
	if(!current_user.is_admin?)
		 redirect_to root_path, notice: t(:no_permissions)
	end
  end
  
  
  
  
  def index
    @infos = Info.all
    
    add_breadcrumb t(:infos), infos_path

  end

  # GET /infos/1
  # GET /infos/1.json
  def show
    @info = Info.find(params[:id])

    add_breadcrumb t(:infos), infos_path
    add_breadcrumb @info.name

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @info }
      format.js
    end
  end

  # GET /infos/new
  # GET /infos/new.json
  def new
    @info = Info.new

    add_breadcrumb t(:infos), infos_path
    add_breadcrumb t(:info_create)

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @info }
    end
  end

  # GET /infos/1/edit
  def edit
    @info = Info.find(params[:id])

    add_breadcrumb t(:infos), infos_path
    add_breadcrumb t(:info_edit)

  end

  # POST /infos
  # POST /infos.json
  def create
    @info = Info.new(params[:info])

    add_breadcrumb t(:infos), infos_path
    add_breadcrumb t(:info_create)

    respond_to do |format|
      if @info.save
        format.html { redirect_to @info, notice: 'Info was successfully created.' }
        format.json { render json: @info, status: :created, location: @info }
      else
        format.html { render action: "new" }
        format.json { render json: @info.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /infos/1
  # PUT /infos/1.json
  def update
    @info = Info.find(params[:id])

    add_breadcrumb t(:infos), infos_path
    add_breadcrumb t(:info_edit)

    respond_to do |format|
      if @info.update_attributes(params[:info])
        format.html { redirect_to @info, notice: 'Info was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @info.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /infos/1
  # DELETE /infos/1.json
  def destroy
    @info = Info.find(params[:id])
    @info.destroy

    respond_to do |format|
      format.html { redirect_to infos_url }
      format.json { head :no_content }
    end
  end
end
