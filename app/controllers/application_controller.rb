class ApplicationController < ActionController::Base
  
  rescue_from DeviseLdapAuthenticatable::LdapException do |exception|
    render :text => exception, :status => 500
  end
  
  
  
  before_filter :authenticate_user!
  before_filter :check_edit_permissions, :only => [:new, :edit, :create, :update, :destroy]
  before_filter :set_main_breadcrumbs
  before_filter :set_zone
  
  

  layout :layout_by_resource
  protected

  def layout_by_resource
    if devise_controller?
      "devise"
    else
      "application"
    end
  end


  protect_from_forgery
  
  #before_filter :set_userdata, :authorize
  

 

  def set_main_breadcrumbs
    add_breadcrumb t(:p_main), root_path
  end


  def get_cur_zone
      current_user.zone_id if current_user
      #7
  end

  def set_zone
	@current_zone = (get_cur_zone && get_cur_zone != 0) ? Zone.find(get_cur_zone).name : t(:zone_not_defined)
  end
  
  def check_edit_permissions
	if(controller_name != "sessions" && !current_user.is_admin?)
		 redirect_to root_path, notice: t(:no_permissions)
	end
  end


  def add_breadcrumb name, url = ''
  	@breadcrumbs ||= []
  	url = eval(url) if url =~ /_path|_url|@/
  	@breadcrumbs << [name, url]
  end

	def date_from_url date_arr = nul
		if date_arr.nil?
			Date.today
		else 
		# Date.new(date_arr["written_on(1i)"].to_i, date_arr["written_on(2i)"].to_i, date_arr["written_on(3i)"].to_i)     			
		Date.new(date_arr[:"written_on(1i)"].to_i,date_arr[:"written_on(2i)"].to_i,date_arr[:"written_on(3i)"].to_i)
		end
	end

  def page_title(title, icon)
    @title = title
    @icon = icon
  end


end
