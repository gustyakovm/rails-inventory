class BladesController < ApplicationController
  # GET /blades
  # GET /blades.json
  before_filter :set_blade_models, :only => [:new, :edit, :create, :update]
  before_filter :set_page_title

  def set_page_title
    page_title("Blade Centers", "icon-building")
  end


  def set_blade_models
    @models = DcatItem.get_by_alias("blade")
	@areas = Area.where(:zone_id => get_cur_zone)
  end

  def index
    add_breadcrumb t(:all_bc), blades_path
    @blades = Blade.where(:zone_id => get_cur_zone).includes(:DcatItem,:rackspace => :area)
  end

  # GET /blades/1
  # GET /blades/1.json
  def show
    @blade = Blade.find(params[:id])
    
    add_breadcrumb t(:all_bc), blades_path
    add_breadcrumb @blade.name
  end

  # GET /blades/new
  # GET /blades/new.json
  def new
    add_breadcrumb t(:all_bc), blades_path
    add_breadcrumb t(:new_bc)

    @blade = Blade.new
  end

  # GET /blades/1/edit
  def edit
    @blade = Blade.find(params[:id])
    add_breadcrumb t(:all_bc), blades_path
    add_breadcrumb @blade.name
  end

  # POST /blades
  # POST /blades.json
  def create
    add_breadcrumb t(:all_bc), blades_path
    add_breadcrumb t(:new_bc)

    @blade = Blade.new(params[:blade])
    @models = DcatItem.get_by_alias("blade")
    @blade.zone_id = get_cur_zone

    respond_to do |format|
      if @blade.save
        format.html { redirect_to @blade, notice: 'Blade was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /blades/1
  # PUT /blades/1.json
  def update
    @blade = Blade.find(params[:id])
    @blade.zone_id = get_cur_zone

    add_breadcrumb t(:all_bc), blades_path
    add_breadcrumb @blade.name

    respond_to do |format|
      if @blade.update_attributes(params[:blade])
        format.html { redirect_to @blade, notice: 'Blade was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @blade.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /blades/1
  # DELETE /blades/1.json
  def destroy
    @blade = Blade.find(params[:id])
    @blade.destroy

    respond_to do |format|
      format.html { redirect_to blades_url }
      format.js

    end
  end
end
