class ReplacePartsController < ApplicationController
  # GET /replace_parts
  # GET /replace_parts.json
  
    before_filter :set_page_title

  def set_page_title
    page_title(t(:replace_parts), "icon-truck")
  end
  
  
  
  
  def index
    @replace_parts = ReplacePart.order("created_at desc").all
	
	add_breadcrumb t :replace_parts, replace_parts_path
	
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @replace_parts }
    end
  end

  # GET /replace_parts/1
  # GET /replace_parts/1.json
  def show
    @replace_part = ReplacePart.find(params[:id])
	
	add_breadcrumb t(:replace_parts), replace_parts_path
    add_breadcrumb @replace_part.network_name
	

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @replace_part }
    end
  end

  # GET /replace_parts/new
  # GET /replace_parts/new.json
  def new
    @replace_part = ReplacePart.new
	
	add_breadcrumb t(:replace_parts), replace_parts_path
    add_breadcrumb t(:new_replace_parts)

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @replace_part }
    end
  end

  # GET /replace_parts/1/edit
  def edit
    @replace_part = ReplacePart.find(params[:id])
	
	add_breadcrumb t(:replace_parts), replace_parts_path
    add_breadcrumb t(:edit_replace_parts)
	
  end

  # POST /replace_parts
  # POST /replace_parts.json
  def create
    @replace_part = ReplacePart.new(params[:replace_part])
	
	add_breadcrumb t(:replace_parts), replace_parts_path
    add_breadcrumb t(:new_replace_parts)

    respond_to do |format|
      if @replace_part.save
        format.html { redirect_to @replace_part, notice: 'Replace part was successfully created.' }
        format.json { render json: @replace_part, status: :created, location: @replace_part }
      else
        format.html { render action: "new" }
        format.json { render json: @replace_part.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /replace_parts/1
  # PUT /replace_parts/1.json
  def update
    @replace_part = ReplacePart.find(params[:id])
	
	add_breadcrumb t(:replace_parts), replace_parts_path
    add_breadcrumb t(:edit_replace_parts)

    respond_to do |format|
      if @replace_part.update_attributes(params[:replace_part])
        format.html { redirect_to @replace_part, notice: 'Replace part was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @replace_part.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /replace_parts/1
  # DELETE /replace_parts/1.json
  def destroy
    @replace_part = ReplacePart.find(params[:id])
    @replace_part.destroy

    respond_to do |format|
      format.html { redirect_to replace_parts_url }
      format.json { head :no_content }
    end
  end
end
