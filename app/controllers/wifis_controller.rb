class WifisController < ApplicationController
  # GET /wifis
  # GET /wifis.json
  before_filter :set_info, :only => [:new, :edit, :create, :update]
  before_filter :set_page_title

  def set_page_title
    page_title(t(:wifi) , "icon-rss")
  end

  def set_info
    @devise_types = DcatItem.get_by_alias("devise_type")
    @devise_owns = DcatItem.get_by_alias("devise_own")
  end



  def index
    add_breadcrumb t :all_wifi, wifis_path
    @wifis = Wifi.includes(:dev_type, :dev_own).all

    respond_to do |format|
      format.html # show.html.erb
      format.xls
    end
  end

  # GET /wifis/1
  # GET /wifis/1.json
  def show
    @wifi = Wifi.find(params[:id])

    add_breadcrumb t(:all_wifi), wifis_path
    add_breadcrumb @wifi.username
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @wifi }
    end
  end

  # GET /wifis/new
  # GET /wifis/new.json
  def new
    add_breadcrumb t(:all_wifi), wifis_path
    add_breadcrumb t(:new_wifi)
    @wifi = Wifi.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @wifi }
    end
  end

  # GET /wifis/1/edit
  def edit
    add_breadcrumb t(:all_wifi), wifis_path
    add_breadcrumb t(:edit_wifi)

    @wifi = Wifi.find(params[:id])
  end

  # POST /wifis
  # POST /wifis.json
  def create
    add_breadcrumb t(:all_wifi), wifis_path
    add_breadcrumb t(:new_wifi)

    @wifi = Wifi.new(params[:wifi])

    respond_to do |format|
      if @wifi.save
        format.html { redirect_to @wifi, notice: 'Wifi was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /wifis/1
  # PUT /wifis/1.json
  def update
    add_breadcrumb t :all_wifi, wifis_path
    add_breadcrumb t(:edit_wifi)
    
    @wifi = Wifi.find(params[:id])

    respond_to do |format|
      if @wifi.update_attributes(params[:wifi])
        format.html { redirect_to @wifi, notice: 'Wifi was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /wifis/1
  # DELETE /wifis/1.json
  def destroy
    @wifi = Wifi.find(params[:id])
    @wifi.destroy

    respond_to do |format|
      format.html { redirect_to wifis_url }
      format.json { head :no_content }
    end
  end
end
