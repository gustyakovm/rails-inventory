class ServersController < ApplicationController
   
  before_filter :set_info_data, :only =>[ :new, :edit, :index ]
  before_filter :set_page_title

  def set_page_title
    page_title(t(:server), "icon-desktop")
  end


  def set_info_data

    @models = DcatItem.get_by_alias("server_model")
    @os = DcatItem.get_by_alias("os")
    @statuses = DcatItem.get_by_alias("condition")
    @s_types = DcatItem.get_by_alias("server_type")
    @os_bits = DcatItem.get_by_alias("os_bit")
    @areas = Area.where(:zone_id => get_cur_zone)
    @rackspaces = Rackspace.get_by_zone(get_cur_zone)

  end

  def index

    @servers = Server.search(params)
    add_breadcrumb t(:server), servers_path

    respond_to do |format|
      format.html
      format.xls
    end
    
  end

  # GET /servers/1
  # GET /servers/1.json
  def show
    @server = Server.find(params[:id])
    add_breadcrumb t(:server), servers_path
    add_breadcrumb @server.name


    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @server }
    end
  end

  # GET /servers/new
  # GET /servers/new.json
  def new
    @server = Server.new

    add_breadcrumb t(:server), servers_path
    add_breadcrumb t(:new_server)

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @server }
    end
  end

  # GET /servers/1/edit
  def edit
    add_breadcrumb t(:server), servers_path
    add_breadcrumb t(:edit_server)

    @server = Server.find(params[:id])
  end

  # POST /servers
  # POST /servers.json
  def create
    @server = Server.new(params[:server])
    @server.zone_id = get_cur_zone

    add_breadcrumb t(:server), servers_path
    add_breadcrumb t(:new_server)
    

    respond_to do |format|
      if @server.save
        format.html { redirect_to @server, notice: 'Server was successfully created.' }
        format.json { render json: @server, status: :created, location: @server }
      else
        format.html { render action: "new" }
        format.json { render json: @server.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /servers/1
  # PUT /servers/1.json
  def update
    @server = Server.find(params[:id])

    add_breadcrumb t(:server), servers_path
    add_breadcrumb t(:edit_server)

    respond_to do |format|
      if @server.update_attributes(params[:server])
        format.html { redirect_to @server, notice: 'Server was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @server.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /servers/1
  # DELETE /servers/1.json
  def destroy
    @server = Server.find(params[:id])
    @server.destroy

    respond_to do |format|
      format.html { redirect_to servers_url }
      format.json { head :no_content }
    end
  end



end
