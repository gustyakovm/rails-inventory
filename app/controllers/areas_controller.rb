class AreasController < ApplicationController
  
  before_filter :set_page_title

  def set_page_title
    page_title(t(:areas), "icon-home")
  end


  def index
    @areas = Area.where(:zone_id => get_cur_zone)
    add_breadcrumb t :all_areas, areas_path

  end

  # GET /areas/1
  # GET /areas/1.json
  def show
    @area = Area.find(params[:id])
    add_breadcrumb t(:all_areas), areas_path
    add_breadcrumb @area.name
  end

  # GET /areas/new
  # GET /areas/new.json
  def new
    @area = Area.new
    add_breadcrumb t(:all_areas), areas_path
    add_breadcrumb t(:new_area)
  end

  # GET /areas/1/edit
  def edit
    @area = Area.find(params[:id])
    add_breadcrumb t(:all_areas), areas_path
    add_breadcrumb t(:edit_area)
  end

  # POST /areas
  # POST /areas.json
  def create
    @area = Area.new(params[:area])
    @area.zone_id = get_cur_zone

    add_breadcrumb t(:all_areas), areas_path
    add_breadcrumb t(:new_area)


    respond_to do |format|
      if @area.save
        format.html { redirect_to areas_path, notice: 'Area was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /areas/1
  # PUT /areas/1.json
  def update
    @area = Area.find(params[:id])

    add_breadcrumb t(:all_areas), areas_path
    add_breadcrumb t(:edit_area)

    respond_to do |format|
      if @area.update_attributes(params[:area])
        format.html { redirect_to areas_path, notice: 'Area was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @area.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /areas/1
  # DELETE /areas/1.json
  def destroy
    @area = Area.find(params[:id])
    @area.destroy

    respond_to do |format|
      format.html { redirect_to areas_url }
      format.json { head :no_content }
    end
  end
end
