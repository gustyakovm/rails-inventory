class MainController < ApplicationController
  skip_before_filter :authorize
  
  def index
  	@serv_types = Server.where(:status_id => Server.in_work, :zone_id => get_cur_zone).includes(:type).group("dcat_items.name").count
  	@tl_count = TapeLibrary.where(:zone_id => get_cur_zone).count
  	@bc_count = Blade.where(:zone_id => get_cur_zone).count
  	@wf_count = Wifi.count

  	@model_types = Server.where(["status_id = ? AND zone_id = ? AND model_id <> ''", Server.in_work, get_cur_zone]).includes(:model).group("dcat_items.name").count
  	@os_bits = Server.where(["status_id = ? AND zone_id = ? ", Server.in_work, get_cur_zone]).includes(:os_b).group("dcat_items.name").count

  end
  
  
  def test	
		require 'ruby-wmi'
  end

end
