class PartsController < ApplicationController
  
  before_filter :get_blade_info, :only => [:index, :new, :edit, :create, :update]
  before_filter :set_page_title

  def set_page_title
    page_title("Blade Centers", "icon-terminal")
  end


  def get_blade_info
    @blade = Blade.find(params[:blade_id])
    @models = DcatItem.get_by_alias("blade_part")
    @servers = Server.get_names
  end


  def index
    add_breadcrumb t(:all_bc), blades_path
    add_breadcrumb t(:all_bp), blade_parts_path(@blade)

    @parts = Part.where(:blade_id => @blade.id).order(:num).all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @parts }
    end
  end

  # GET /parts/1
  # GET /parts/1.json
  def show
    @part = Part.find(params[:id])

    add_breadcrumb t(:all_bp), blade_parts_path(@blade)
    add_breadcrumb t(:blade) + " " + @part.num
  end

  # GET /parts/new
  # GET /parts/new.json
  def new
    @part = Part.new
    add_breadcrumb t(:all_bp), blade_parts_path(@blade)
    add_breadcrumb t(:new_bp)
  end

  # GET /parts/1/edit
  def edit
    @part = Part.find(params[:id])
    add_breadcrumb t(:all_bp), blade_parts_path(@blade)
    add_breadcrumb t(:edit_bp)
  end

  # POST /parts
  # POST /parts.json
  def create
    add_breadcrumb t(:all_bp), blade_parts_path(@blade)
    add_breadcrumb t(:new_bp)

    @part = Part.new(params[:part])
    @part.blade_id = @blade.id

    respond_to do |format|
      if @part.save
        format.html { redirect_to blade_parts_path(@part.blade_id), notice: 'Part was successfully created.' }
        format.json { render json: @part, status: :created, location: @part }
      else
        format.html { render action: "new" }
        format.json { render json: @part.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /parts/1
  # PUT /parts/1.json
  def update

    add_breadcrumb t(:all_bp), blade_parts_path(@blade)
    add_breadcrumb t(:edit_bp)

    @part = Part.find(params[:id])

    respond_to do |format|
      if @part.update_attributes(params[:part])
        format.html { redirect_to blade_parts_path(@part.blade_id), notice: 'Part was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @part.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /parts/1
  # DELETE /parts/1.json
  def destroy
    @part = Part.find(params[:id])
    @part.destroy

    respond_to do |format|
      format.html { redirect_to blade_parts_url(@part.blade_id) }
      format.json { head :no_content }
    end
  end
end
