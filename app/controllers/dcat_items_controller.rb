class DcatItemsController < ApplicationController
  # GET /dcat_items
  # GET /dcat_items.json

  before_filter :get_dcat_info
  before_filter :set_page_title

  def set_page_title
    page_title(t(:dictionary), "icon-pencil")
  end


  def get_dcat_info
    @dcat = Dcat.find(params[:dcat_id])
  end

  def index
    @dcat_items = @dcat.DcatItems
    add_breadcrumb t(:all_cats), dcats_path
    add_breadcrumb @dcat.name
    # @dcat_items = DcatItem.all
  end

  
  def show
    @dcat_item = DcatItem.find(params[:id])
    add_breadcrumb t(:all_cats), dcats_path
    add_breadcrumb @dcat.name, dcat_dcat_items_path(@dcat)
  end

  # GET /dcat_items/new
  # GET /dcat_items/new.json
  def new
    @dcat_item = DcatItem.new

    add_breadcrumb t(:all_cats), dcats_path
    add_breadcrumb @dcat.name, dcat_dcat_items_path(@dcat)
    add_breadcrumb t(:new_cats)



    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @dcat_item }
    end
  end

  # GET /dcat_items/1/edit
  def edit
    @dcat_item = DcatItem.find(params[:id])

    add_breadcrumb t(:all_cats), dcats_path
    add_breadcrumb @dcat.name, dcat_dcat_items_path(@dcat)
    add_breadcrumb t(:edit_cats)

  end

  # POST /dcat_items
  # POST /dcat_items.json
  def create
    @dcat_item = @dcat.DcatItems.new(params[:dcat_item])

    add_breadcrumb t(:all_cats), dcats_path
    add_breadcrumb @dcat.name, dcat_dcat_items_path(@dcat)
    add_breadcrumb t(:new_cats)

    respond_to do |format|
      if @dcat_item.save
        format.html { redirect_to dcat_dcat_items_path(@dcat), notice: 'Dcat item was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /dcat_items/1
  # PUT /dcat_items/1.json
  def update
    @dcat_item = @dcat.DcatItems.find(params[:id])
    add_breadcrumb t(:all_cats), dcats_path
    add_breadcrumb @dcat.name, dcat_dcat_items_path(@dcat)
    add_breadcrumb t(:edit_cats)

    respond_to do |format|
      if @dcat_item.update_attributes(params[:dcat_item])
        format.html { redirect_to dcat_dcat_items_path(@dcat), notice: 'Dcat item was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /dcat_items/1
  # DELETE /dcat_items/1.json
  def destroy
    @dcat_item = @dcat.DcatItems.find(params[:id])
    @dcat_item.destroy

    respond_to do |format|
      format.html { redirect_to dcat_dcat_items_path(@dcat) }
    end
  end
end
