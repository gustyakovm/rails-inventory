class CommutatorsController < ApplicationController
  # GET /commutators
  # GET /commutators.json
  def index
    @commutators = Commutator.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @commutators }
    end
  end

  # GET /commutators/1
  # GET /commutators/1.json
  def show
    @commutator = Commutator.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @commutator }
    end
  end

  # GET /commutators/new
  # GET /commutators/new.json
  def new
    @commutator = Commutator.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @commutator }
    end
  end

  # GET /commutators/1/edit
  def edit
    @commutator = Commutator.find(params[:id])
  end

  # POST /commutators
  # POST /commutators.json
  def create
    @commutator = Commutator.new(params[:commutator])

    respond_to do |format|
      if @commutator.save
        format.html { redirect_to @commutator, notice: 'Commutator was successfully created.' }
        format.json { render json: @commutator, status: :created, location: @commutator }
      else
        format.html { render action: "new" }
        format.json { render json: @commutator.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /commutators/1
  # PUT /commutators/1.json
  def update
    @commutator = Commutator.find(params[:id])

    respond_to do |format|
      if @commutator.update_attributes(params[:commutator])
        format.html { redirect_to @commutator, notice: 'Commutator was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @commutator.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /commutators/1
  # DELETE /commutators/1.json
  def destroy
    @commutator = Commutator.find(params[:id])
    @commutator.destroy

    respond_to do |format|
      format.html { redirect_to commutators_url }
      format.json { head :no_content }
    end
  end
end
