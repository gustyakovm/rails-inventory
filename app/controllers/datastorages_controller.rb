class DatastoragesController < ApplicationController
  # GET /datastorages
  # GET /datastorages.json
  def index
    @datastorages = Datastorage.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @datastorages }
    end
  end

  # GET /datastorages/1
  # GET /datastorages/1.json
  def show
    @datastorage = Datastorage.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @datastorage }
    end
  end

  # GET /datastorages/new
  # GET /datastorages/new.json
  def new
    @datastorage = Datastorage.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @datastorage }
    end
  end

  # GET /datastorages/1/edit
  def edit
    @datastorage = Datastorage.find(params[:id])
  end

  # POST /datastorages
  # POST /datastorages.json
  def create
    @datastorage = Datastorage.new(params[:datastorage])

    respond_to do |format|
      if @datastorage.save
        format.html { redirect_to @datastorage, notice: 'Datastorage was successfully created.' }
        format.json { render json: @datastorage, status: :created, location: @datastorage }
      else
        format.html { render action: "new" }
        format.json { render json: @datastorage.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /datastorages/1
  # PUT /datastorages/1.json
  def update
    @datastorage = Datastorage.find(params[:id])

    respond_to do |format|
      if @datastorage.update_attributes(params[:datastorage])
        format.html { redirect_to @datastorage, notice: 'Datastorage was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @datastorage.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /datastorages/1
  # DELETE /datastorages/1.json
  def destroy
    @datastorage = Datastorage.find(params[:id])
    @datastorage.destroy

    respond_to do |format|
      format.html { redirect_to datastorages_url }
      format.json { head :no_content }
    end
  end
end
