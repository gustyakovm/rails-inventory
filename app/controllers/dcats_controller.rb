class DcatsController < ApplicationController
  # GET /dcats
  # GET /dcats.json

  before_filter :set_page_title

  def set_page_title
    page_title(t(:dictionary), "icon-pencil")
  end


  def index
    @dcats = Dcat.all
    add_breadcrumb t :all_cats, dcats_path
  end

  # GET /dcats/1
  # GET /dcats/1.json
  def show
    @dcat = Dcat.find(params[:id])
    
    add_breadcrumb t(:all_cats), dcats_path
    add_breadcrumb @dcat.name

  end

  # GET /dcats/new
  # GET /dcats/new.json
  def new
    @dcat = Dcat.new
    add_breadcrumb t(:all_cats), dcats_path
    add_breadcrumb t :new_cats

  end

  # GET /dcats/1/edit
  def edit
    @dcat = Dcat.find(params[:id])
    add_breadcrumb t(:all_cats), dcats_path
    add_breadcrumb t :edit_cats

  end

  # POST /dcats
  # POST /dcats.json
  def create
    @dcat = Dcat.new(params[:dcat])
    add_breadcrumb t(:all_cats), dcats_path
    add_breadcrumb t :new_cats


    respond_to do |format|
      if @dcat.save
        format.html { redirect_to @dcat, notice: 'Dcat was successfully created.' }
        format.json { render json: @dcat, status: :created, location: @dcat }
      else
        format.html { render action: "new" }
        format.json { render json: @dcat.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /dcats/1
  # PUT /dcats/1.json
  def update
    @dcat = Dcat.find(params[:id])
    add_breadcrumb t(:all_cats), dcats_path
    add_breadcrumb t :edit_cats


    respond_to do |format|
      if @dcat.update_attributes(params[:dcat])
        format.html { redirect_to @dcat, notice: 'Dcat was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @dcat.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dcats/1
  # DELETE /dcats/1.json
  def destroy
    @dcat = Dcat.find(params[:id])
    @dcat.destroy

    respond_to do |format|
      format.html { redirect_to dcats_url }
      format.json { head :no_content }
    end
  end
end
