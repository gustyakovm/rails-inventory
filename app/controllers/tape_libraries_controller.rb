class TapeLibrariesController < ApplicationController

  before_filter :set_tl_info, :only => [:new, :edit, :create, :update]
  before_filter :set_page_title

  def set_page_title
    page_title(t(:all_tl), "icon-sun")
  end

  def set_tl_info
    @models = DcatItem.get_by_alias("tape_library")
    @areas = Area.all
    @rackspaces = Rackspace.get_by_zone(get_cur_zone)

  end

  def index
    add_breadcrumb t :all_tl, tape_libraries_path
    @tape_libraries = TapeLibrary.where(:zone_id => get_cur_zone)
  end

  # GET /tape_libraries/1
  # GET /tape_libraries/1.json
  def show
    @tape_library = TapeLibrary.find(params[:id])

    add_breadcrumb t(:all_tl), tape_libraries_path
    add_breadcrumb @tape_library.name

  end

  # GET /tape_libraries/new
  # GET /tape_libraries/new.json
  def new
    add_breadcrumb t(:all_tl), tape_libraries_path
    add_breadcrumb t :new_tl

    @tape_library = TapeLibrary.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @tape_library }
    end
  end

  # GET /tape_libraries/1/edit
  def edit
    add_breadcrumb t(:all_tl), tape_libraries_url
    add_breadcrumb t :edit_tl

    @tape_library = TapeLibrary.find(params[:id])
  end

  # POST /tape_libraries
  # POST /tape_libraries.json
  def create
    add_breadcrumb t(:all_tl), tape_libraries_url
    add_breadcrumb t :new_tl

    @tape_library = TapeLibrary.new(params[:tape_library])
    @tape_library.zone_id = get_cur_zone

    respond_to do |format|
      if @tape_library.save
        format.html { redirect_to @tape_library, notice: 'Tape library was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /tape_libraries/1
  # PUT /tape_libraries/1.json
  def update

    add_breadcrumb t(:all_tl), tape_libraries_url
    add_breadcrumb t :edit_tl

    @tape_library = TapeLibrary.find(params[:id])
    @tape_library.zone_id = get_cur_zone

    respond_to do |format|
      if @tape_library.update_attributes(params[:tape_library])
        format.html { redirect_to @tape_library, notice: 'Tape library was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /tape_libraries/1
  # DELETE /tape_libraries/1.json
  def destroy
    @tape_library = TapeLibrary.find(params[:id])
    @tape_library.destroy

    respond_to do |format|
      format.html { redirect_to tape_libraries_url }
      format.json { head :no_content }
    end
  end
end
