class RackspacesController < ApplicationController
  # GET /rackspaces
  # GET /rackspaces.json
  before_filter :get_area_info
  before_filter :set_page_title

  def set_page_title
    page_title(t(:rackspaces), "icon-home")
  end

  def get_area_info
    @area = Area.find(params[:area_id])
  end

  def index
    @rackspaces = Rackspace.where(:area_id => params[:area_id])
    add_breadcrumb t(:areas), areas_path
    add_breadcrumb @area.name

  end

  # GET /rackspaces/1
  # GET /rackspaces/1.json
  def show
    @rackspace = Rackspace.find(params[:id])

    add_breadcrumb t(:areas), areas_path
    add_breadcrumb @area.name, area_rackspaces_path(@rackspace.area_id)
    add_breadcrumb @rackspace.name


  end

  # GET /rackspaces/new
  # GET /rackspaces/new.json
  def new
    @rackspace = Rackspace.new
    add_breadcrumb t(:areas), areas_path
    add_breadcrumb @area.name, area_rackspaces_path(@area)
    add_breadcrumb t(:new_rackspace)

  end

  # GET /rackspaces/1/edit
  def edit
    @rackspace = Rackspace.find(params[:id])

    add_breadcrumb t(:areas), areas_path
    add_breadcrumb @area.name, area_rackspaces_path(@area)
    add_breadcrumb t(:edit_rackspace)

  end

  # POST /rackspaces
  # POST /rackspaces.json
  def create
    @rackspace = Rackspace.new(params[:rackspace])
    @rackspace.area_id = params[:area_id]

    add_breadcrumb t(:areas), areas_path
    add_breadcrumb @area.name, area_rackspaces_path(@area)
    add_breadcrumb t(:new_rackspace)

    respond_to do |format|
      if @rackspace.save
        format.html { redirect_to area_rackspaces_path(@rackspace.area_id), notice: 'Rackspace was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /rackspaces/1
  # PUT /rackspaces/1.json
  def update
    @rackspace = Rackspace.find(params[:id])
    @rackspace.area_id = params[:area_id]

    add_breadcrumb t(:areas), areas_path
    add_breadcrumb @area.name, area_rackspaces_path(@area)
    add_breadcrumb t(:edit_rackspace)


    respond_to do |format|
      if @rackspace.update_attributes(params[:rackspace])
        format.html { redirect_to area_rackspaces_path(@rackspace.area_id), notice: 'Rackspace was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /rackspaces/1
  # DELETE /rackspaces/1.json
  def destroy
    @rackspace = Rackspace.find(params[:id])
    @rackspace.destroy

    respond_to do |format|
      format.html { redirect_to area_rackspaces_path(@rackspace.area_id) }
    end
  end
end
