class ZonesController < ApplicationController
  before_filter :set_page_title

  def set_page_title
    page_title(t(:zones), "icon-plane")
  end


  def index
    @zones = Zone.all
    add_breadcrumb t(:zones), zones_path
  end

  # GET /zones/1
  # GET /zones/1.json
  def show
    @zone = Zone.find(params[:id])
    add_breadcrumb t(:zones), zones_path
    add_breadcrumb @zone.name

  end

  # GET /zones/new
  # GET /zones/new.json
  def new
    @zone = Zone.new
    add_breadcrumb t(:zones), zones_path
    add_breadcrumb t(:new_zone)


  end

  # GET /zones/1/edit
  def edit
    @zone = Zone.find(params[:id])
    add_breadcrumb t(:zones), zones_path
    add_breadcrumb t(:edit_zone)
  end

  # POST /zones
  # POST /zones.json
  def create
    @zone = Zone.new(params[:zone])
    add_breadcrumb t(:zones), zones_path
    add_breadcrumb t(:new_zone)


    respond_to do |format|
      if @zone.save
        format.html { redirect_to zones_path, notice: 'Zone was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /zones/1
  # PUT /zones/1.json
  def update
    @zone = Zone.find(params[:id])
    add_breadcrumb t(:zones), zones_path
    add_breadcrumb t(:edit_zone)


    respond_to do |format|
      if @zone.update_attributes(params[:zone])
        format.html { redirect_to @zone, notice: 'Zone was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @zone.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /zones/1
  # DELETE /zones/1.json
  def destroy
    @zone = Zone.find(params[:id])
    @zone.destroy

    respond_to do |format|
      format.html { redirect_to zones_url }
      format.json { head :no_content }
    end
  end
end
