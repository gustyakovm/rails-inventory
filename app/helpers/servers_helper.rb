module ServersHelper

	def status_by_id id

		case id
			when 16
				status = "success"	
			when 17
				status = "danger"
			else 
				status = "warning"
		end
		status
	end

	def text_for_excel text
		text.gsub("\r\n", '&#13;').html_safe
	end

end
