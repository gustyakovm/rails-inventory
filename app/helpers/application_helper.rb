module ApplicationHelper

	def show_breadcrumbs
		render 'layouts/blocks/breadcrumbs'
	end

	def print_flex arr
  		puts YAML::dump(arr)
  end

  def v_separator size
  	simple_format("&nbsp;", :style => "margin-top:#{size}px")
  end

  def title(page_title)
    content_for(:title) { page_title }
  end


end
