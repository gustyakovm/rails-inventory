tinymce.init({
    selector: ".tinymce",
    forced_root_block : false,
    force_br_newlines : true,
		force_p_newlines : false,
		menubar: false
 });
