# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

jQuery ->
	$('#server_date_start').datepicker
		dateFormat: 'yy-mm-dd'

	$('input.star').rating()
	change_serv_type()
	
	$('#server_type_id').change ->
		change_serv_type()


change_serv_type =() ->
	id = $('#server_type_id').val()

	if(id == "22" || id == "") 
		$("#server_rackspace_id").prop('disabled', false)
		$("#server_model_id").prop('disabled', false)
		$("#server_model_details").prop('disabled', false)
		$("#server_serial_num").prop('disabled', false)
		$("#server_inventory_num").prop('disabled', false)
		$("#filds_by_type").slideDown();
	else
		$("#server_rackspace_id").prop('disabled', true)
		$("#server_model_id").prop('disabled', true)
		$("#server_model_details").prop('disabled', true)
		$("#server_serial_num").prop('disabled', true)
		$("#server_inventory_num").prop('disabled', true)
		$("#filds_by_type").slideUp();

