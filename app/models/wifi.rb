class Wifi < ActiveRecord::Base
  attr_accessible :devise_own_id, :devise_type_id, :mac, :model, :username

  validates :username, presence: true
  validates :devise_own_id, presence: true
  validates :devise_type_id, presence: true
  validates :mac, presence: true
  validates :model, presence: true

  belongs_to :dev_type, :class_name => "DcatItem", foreign_key: "devise_type_id"
  belongs_to :dev_own, :class_name => "DcatItem", foreign_key: "devise_own_id"

  default_scope order(:username)

end
