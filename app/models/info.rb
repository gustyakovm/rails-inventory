class Info < ActiveRecord::Base
  attr_accessible :cat_id, :description, :name

  validates :name, presence: true
  validates :cat_id, presence: true

  belongs_to :category, :class_name => "DcatItem", foreign_key: "cat_id"
end
