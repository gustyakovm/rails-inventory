class DcatItem < ActiveRecord::Base
	
  default_scope order("dcat_items.name")
  attr_accessible :cat_id, :name
  validates :name, presence: true

  belongs_to :Dcat
  has_many :blades
  has_many :tape_libraries, dependent: :nullify, foreign_key: "model_id"
  has_many :wifis, dependent: :nullify, foreign_key: "devise_type_id"
  has_many :wifis, dependent: :nullify, foreign_key: "devise_own_id"

  has_many :servers, dependent: :nullify, foreign_key: "model_id"
  has_many :servers, dependent: :nullify, foreign_key: "os_id"
  has_many :servers, dependent: :nullify, foreign_key: "status_id"
  has_many :servers, dependent: :nullify, foreign_key: "type_id"
  has_many :servers, dependent: :nullify, foreign_key: "os_bit"
  

  has_many :parts, dependent: :nullify, foreign_key: "model_id"
  has_many :infos, dependent: :nullify, foreign_key: "cat_id"



  scope :get_by_alias, lambda {|alias_str| all(:include => :Dcat, :conditions => { :dcats => {:alias => alias_str}})} 
end
