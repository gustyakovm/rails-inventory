class Dcat < ActiveRecord::Base
  attr_accessible :alias, :name
  has_many :DcatItems, dependent: :destroy

  validates :name, presence: true
  validates :alias, presence: true
end
