class ReplacePart < ActiveRecord::Base
  attr_accessible :date_getting, :date_sending, :descr_getting, :descr_sending, :detail_type, :network_name
  default_scope order("created_at desc")
end
