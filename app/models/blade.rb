class Blade < ActiveRecord::Base
  attr_accessible :ip, :name, :model_id, :zone_id, :inventory_num, :rackspace_id
  validates :ip, presence: true
  validates :name, presence: true
  validates :model_id, presence: true
  validates :zone_id, presence: true
  validates :rackspace_id, presence: true
 

  belongs_to :zone
  belongs_to :rackspace
  belongs_to :DcatItem,  foreign_key: "model_id"
  has_many :parts
end
