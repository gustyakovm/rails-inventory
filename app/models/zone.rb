class Zone < ActiveRecord::Base
  attr_accessible :name, :alias
  validates :name, presence: true
  validates :alias, presence: true
  has_many :areas
  has_many :blades
  has_many :tape_libraries, dependent: :nullify

end
