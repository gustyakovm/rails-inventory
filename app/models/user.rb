require 'devise'

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :ldap_authenticatable, :rememberable, :trackable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :login, :email, :password, :password_confirmation, :remember_me, :displayname, :zone_id, :is_admin
  # attr_accessible :title, :body
  
  before_save :get_ldap_displayname, :get_ldap_groups
  
  
  def get_ldap_displayname
	 #Rails::logger.info("### Getting the users first name")
      self.displayname = Devise::LDAP::Adapter.get_ldap_param(self.login,"displayname").first
  end
  
  def get_ldap_groups
	cur_zone = 0
	is_admin = false
	zs = Zone.all
	groups = Devise::LDAP::Adapter.get_ldap_param(self.login,"memberOf")
	
	groups.each do |group|
		val = group.match("CN=(.+?)\,")[1]
		if(val == "HardwareInventoryAdmins")
			is_admin = true
		else
			zs.each do |z|
				if(z.alias == val)
					cur_zone = z.id
				end
			end
		end
	end
	self.is_admin = is_admin
	self.zone_id = cur_zone
  end
  
  
end


