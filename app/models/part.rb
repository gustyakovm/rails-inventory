class Part < ActiveRecord::Base
  attr_accessible :blade_id, :model_id, :num, :server_id

  validates :blade_id, presence: true
  validates :model_id, presence: true
  validates :num, presence: true, :numericality => true

  belongs_to :blade

  belongs_to :model, :class_name => "DcatItem", foreign_key: "model_id"
  belongs_to :server

end
