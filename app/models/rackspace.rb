class Rackspace < ActiveRecord::Base
  attr_accessible :area_id, :name
  validates :area_id, presence: true
  validates :name, presence: true
  belongs_to :area
  has_many :tape_libraries, dependent: :nullify
  has_many :servers, dependent: :nullify

  default_scope order(:name)
  scope :get_by_zone, lambda {|zone_id| all(:include => :area, :conditions => { :areas => {:zone_id => zone_id}})} 
end
