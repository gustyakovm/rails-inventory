class Area < ActiveRecord::Base
  attr_accessible :name, :zone_id
  belongs_to :zone
  has_many :rackspaces, dependent: :nullify
end
