class Server < ActiveRecord::Base
  FIZ_TYPE = 22
  IN_WORK = 16
  OFF_WORK = 17

  attr_accessible :description, :date_start, :date_end, :hardware, :inventory_num, :type_id, :os_bit, :ip, :mac, :model_details, :model_id, :name, :os_details, :os_id, :rackspace_id, :rating, :serial_num, :software, :status_id, :zone_id
  
  validates :name, presence: true, :uniqueness => true
  validates :rating, presence: true
  validates :type_id, presence: true
  #validates :os_bit, presence: true
  
  # validates_inclusion_of :date_start,
  #     :in => Date.civil(2000, 1, 1)..Date.today,
  #     :message => "Must be between 2000 and now",
  #     :allow_blank => true

  

  belongs_to :model, :class_name => "DcatItem", foreign_key: "model_id"
  belongs_to :os, :class_name => "DcatItem", foreign_key: "os_id"
  belongs_to :status, :class_name => "DcatItem", foreign_key: "status_id"
  belongs_to :type, :class_name => "DcatItem", foreign_key: "type_id"
  belongs_to :os_b, :class_name => "DcatItem", foreign_key: "os_bit"
  belongs_to :rackspace
  has_one :part
  
  
  before_save :set_fields_by_type
  before_save :date_end_by_status #если не в работе- поставить дату окончания

  scope :get_names, select("id, name")
  #default_scope order("servers.name")

  def self.search(params)
  		servers = Server.includes(:model, :os, :status, :rackspace => :area)
	  	servers = servers.where("servers.name like ?", "%#{params[:name]}%") if params[:name].present?
      servers = servers.where(:type_id => params[:server_type_id]) if params[:server_type_id].present?
	  	servers = servers.where(:model_id => params[:server_model_id]) if params[:server_model_id].present?
	  	servers = servers.where(:os_bit => params[:os_bit]) if params[:os_bit].present?
      servers = servers.where("os_id IN (?)", params[:os]) if params[:os].present?
      servers = servers.where(:rating => params[:rating]) if params[:rating].present?

	servers = servers.where(:status_id => params[:status_id]) if params[:status_id].present?
      servers = servers.where("areas.id IN (?)", params[:areas]) if params[:areas].present?
      servers = servers.order( params[:sort_by].present? ? "#{params[:sort_by]}" : "servers.name")
	  	servers
  end

  def set_fields_by_type
    if(type_id == 23)
      self.model_id = nil
      self.model_details = nil
      self.rackspace_id = nil
      self.serial_num = nil
      self.inventory_num = nil
    end
  end
  
  
  def date_end_by_status
	if(status_id == Server::OFF_WORK)
		self.date_end = Date.today
	else
		self.date_end = nil
	end
  end

  def self.in_work
  	16
  end

end
