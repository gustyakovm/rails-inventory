# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140113124406) do

  create_table "areas", :force => true do |t|
    t.string   "name"
    t.integer  "zone_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "blades", :force => true do |t|
    t.string   "name"
    t.string   "ip"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "zone_id"
    t.integer  "model_id"
    t.string   "inventory_num"
    t.integer  "rackspace_id"
  end

  create_table "commutators", :force => true do |t|
    t.string   "name"
    t.integer  "model_id"
    t.string   "serial_num"
    t.string   "inventory_num"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "datastorages", :force => true do |t|
    t.string   "name"
    t.integer  "zone_id"
    t.integer  "rackspace_id"
    t.integer  "status_id"
    t.text     "extenders"
    t.text     "disks"
    t.string   "inventory_num"
    t.integer  "model_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "dcat_items", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "dcat_id"
  end

  create_table "dcats", :force => true do |t|
    t.string   "name"
    t.string   "alias"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "groups", :force => true do |t|
    t.string   "name",       :limit => 20
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "groups_sub_groups", :id => false, :force => true do |t|
    t.integer "group_id"
    t.integer "sub_group_id"
  end

  create_table "histories", :force => true do |t|
    t.integer  "user_id"
    t.string   "message"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "infos", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "cat_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "items", :force => true do |t|
    t.string   "name",          :limit => 50
    t.text     "description"
    t.integer  "group_id"
    t.integer  "rackspace_id"
    t.string   "ip",            :limit => 50
    t.string   "mac",           :limit => 50
    t.string   "serial_num",    :limit => 50
    t.string   "inventory_num", :limit => 50
    t.date     "date_start"
    t.date     "date_end"
    t.integer  "rating"
    t.boolean  "is_virtual"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "parts", :force => true do |t|
    t.integer  "model_id"
    t.integer  "num"
    t.integer  "blade_id"
    t.integer  "server_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "rackshelves", :force => true do |t|
    t.string   "name"
    t.integer  "area_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "rackspaces", :force => true do |t|
    t.string   "name"
    t.integer  "area_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "replace_parts", :force => true do |t|
    t.string   "network_name"
    t.string   "detail_type"
    t.date     "date_sending"
    t.date     "date_getting"
    t.text     "descr_sending"
    t.text     "descr_getting"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "servers", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "zone_id"
    t.integer  "rackspace_id"
    t.string   "ip"
    t.string   "mac"
    t.string   "serial_num"
    t.string   "inventory_num"
    t.integer  "rating"
    t.string   "model_id"
    t.string   "model_details"
    t.string   "os_id"
    t.string   "os_details"
    t.text     "hardware"
    t.text     "software"
    t.integer  "status_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "type_id"
    t.integer  "os_bit"
    t.date     "date_start"
    t.date     "date_end"
  end

  create_table "sub_groups", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "tape_libraries", :force => true do |t|
    t.string   "name"
    t.string   "ip"
    t.integer  "zone_id"
    t.integer  "rackspace_id"
    t.integer  "model_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "inventory_num"
  end

  create_table "users", :force => true do |t|
    t.string   "login",               :limit => 100
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",  :limit => 100
    t.string   "last_sign_in_ip",     :limit => 100
    t.datetime "created_at",                                            :null => false
    t.datetime "updated_at",                                            :null => false
    t.string   "displayname",         :limit => 100
    t.boolean  "is_admin",                           :default => false
    t.integer  "zone_id"
  end

  add_index "users", ["login"], :name => "index_users_on_login", :unique => true

  create_table "wifis", :force => true do |t|
    t.string   "username"
    t.string   "model"
    t.string   "mac"
    t.integer  "devise_type_id"
    t.integer  "devise_own_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "zones", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "alias"
  end

end
