class AddOsBitInServerTable < ActiveRecord::Migration
  def up
  	add_column :servers, :os_bit, :integer
  end

  def down
  	remove_column :servers, :os_bit
  end
end
