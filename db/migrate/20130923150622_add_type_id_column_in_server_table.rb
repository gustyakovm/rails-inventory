class AddTypeIdColumnInServerTable < ActiveRecord::Migration
  def up
  	add_column :servers, :type_id, :integer
  end

  def down
  	remove_column :servers, :type_id
  end
end
