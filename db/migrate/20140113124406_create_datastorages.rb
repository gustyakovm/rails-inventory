class CreateDatastorages < ActiveRecord::Migration
  def change
    create_table :datastorages do |t|
      t.string :name
      t.integer :zone_id
      t.integer :rackspace_id
      t.integer :status_id
      t.text :extenders
      t.text :disks
      t.string :inventory_num
      t.integer :model_id

      t.timestamps
    end
  end
end
