class CreateAreas < ActiveRecord::Migration
  def change
  	drop_table :areas
    create_table :areas do |t|
      t.string :name
      t.integer :zone_id

      t.timestamps
    end
  end
end
