class CreateReplaceParts < ActiveRecord::Migration
  def change
    create_table :replace_parts do |t|
      t.string :network_name
      t.string :detail_type
      t.date :date_sending
      t.date :date_getting
      t.text :descr_sending
      t.text :descr_getting

      t.timestamps
    end
  end
end
