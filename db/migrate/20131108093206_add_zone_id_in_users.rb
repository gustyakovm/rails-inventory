class AddZoneIdInUsers < ActiveRecord::Migration
  def up
  	add_column :users, :zone_id, :integer
	remove_column :users, :groups
  end

  def down
  	remove_column :users, :zone_id
	add_column :users, :groups, :text
  end
end
