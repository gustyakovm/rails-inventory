class AddGroupColToUsers < ActiveRecord::Migration
   def up
  	add_column :users, :groups, :text
  end

  def down
  	remove_column :users, :groups
  end
end
