class CreateServers < ActiveRecord::Migration
  def change
    create_table :servers do |t|
      t.string :name
      t.text :description
      t.integer :zone_id
      t.integer :rackspace_id
      t.string :ip
      t.string :mac
      t.string :serial_num
      t.string :inventory_num
      t.string :rating
      t.string :model_id
      t.string :model_details
      t.string :os_id
      t.string :os_details
      t.text :hardware
      t.text :software
      t.integer :status_id

      t.timestamps
    end
  end
end
