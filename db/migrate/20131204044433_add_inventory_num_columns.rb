class AddInventoryNumColumns < ActiveRecord::Migration
  def up
	add_column :tape_libraries, :inventory_num, :string
	add_column :blades, :inventory_num, :string
	add_column :blades, :rackspace_id, :integer
  end

  def down
	remove_column :tape_libraries, :inventory_num
	remove_column :blades, :inventory_num
	remove_column :blades, :rackspace_id
  end
end
