class ChangeDcatItemsCatId < ActiveRecord::Migration
  def up
  	add_column :dcat_items, :dcat_id, :integer
  	remove_column  :dcat_items, :cat_id
  end

  def down
  	add_column :dcat_items, :cat_id, :integer
  	remove_column  :dcat_items, :dcat_id
  end
end
