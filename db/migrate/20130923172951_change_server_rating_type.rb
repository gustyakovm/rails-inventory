class ChangeServerRatingType < ActiveRecord::Migration
  def up
  	change_column :servers, :rating, :integer
  end

  def down
  	change_column :servers, :rating, :string
  end
end
