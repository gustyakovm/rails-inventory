class CreateTapeLibraries < ActiveRecord::Migration
  def change
    create_table :tape_libraries do |t|
      t.string :name
      t.string :ip
      t.integer :zone_id
      t.integer :rackspace_id
      t.integer :model_id

      t.timestamps
    end
  end
end
