class AddAliasColumnInZones < ActiveRecord::Migration
   def up
  	add_column :zones, :alias, :string
  end

  def down
  	remove_column :zones, :alias
  end
  
end
