class AddDateEndInServers < ActiveRecord::Migration
   def up
  	add_column :servers, :date_end, :date
  end

  def down
  	remove_column :servers, :date_end
  end
end
