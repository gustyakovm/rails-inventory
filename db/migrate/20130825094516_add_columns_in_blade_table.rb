class AddColumnsInBladeTable < ActiveRecord::Migration
  def up
  	add_column :blades, :zone_id, :integer
  	add_column :blades, :model_id, :integer
  end

  def down
  	remove_column  :blades, :zone_id
  	remove_column  :blades, :model_id

  end
end
