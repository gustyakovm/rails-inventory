class CreateDcatItems < ActiveRecord::Migration
  def change
    create_table :dcat_items do |t|
      t.string :name
      t.integer :cat_id

      t.timestamps
    end
  end
end
