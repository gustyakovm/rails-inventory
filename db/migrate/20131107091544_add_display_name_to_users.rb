class AddDisplayNameToUsers < ActiveRecord::Migration
  def up
  	add_column :users, :displayname, :string
  end

  def down
  	remove_column :users, :displayname
  end
end
