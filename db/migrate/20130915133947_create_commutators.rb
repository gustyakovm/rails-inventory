class CreateCommutators < ActiveRecord::Migration
  def change
    create_table :commutators do |t|
      t.string :name
      t.integer :model_id
      t.string :serial_num
      t.string :inventory_num

      t.timestamps
    end
  end
end
