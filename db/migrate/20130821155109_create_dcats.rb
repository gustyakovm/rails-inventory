class CreateDcats < ActiveRecord::Migration
  def change
    create_table :dcats do |t|
      t.string :name
      t.string :alias

      t.timestamps
    end
  end
end
