class CreateParts < ActiveRecord::Migration
  def change
    create_table :parts do |t|
      t.string :name
      t.integer :model_id
      t.integer :num
      t.integer :blade_id
      t.integer :server_id

      t.timestamps
    end
  end
end
