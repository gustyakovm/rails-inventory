class AddColumnDateStartInServers < ActiveRecord::Migration
  def up
  	add_column :servers, :date_start, :date
  end

  def down
  	remove_column :servers, :date_start
  end
end
