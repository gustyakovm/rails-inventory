class CreateRackspaces < ActiveRecord::Migration
  def change
    create_table :rackspaces do |t|
      t.string :name
      t.integer :area_id

      t.timestamps
    end
  end
end
