# ************************************************************
# Sequel Pro SQL dump
# Версия 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Адрес: localhost (MySQL 5.5.25)
# Схема: rails_inventory
# Время создания: 2013-10-09 11:47:15 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Дамп таблицы areas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `areas`;

CREATE TABLE `areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `areas` WRITE;
/*!40000 ALTER TABLE `areas` DISABLE KEYS */;

INSERT INTO `areas` (`id`, `name`, `zone_id`, `created_at`, `updated_at`)
VALUES
	(1,'Аппаратный зал',7,'2013-08-22 19:27:12','2013-08-22 19:27:12'),
	(2,'ЛАЗ',7,'2013-08-22 19:28:38','2013-08-22 19:28:38'),
	(3,'РПДУ',7,'2013-10-07 15:30:40','2013-10-07 15:30:40');

/*!40000 ALTER TABLE `areas` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы blades
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blades`;

CREATE TABLE `blades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `blades` WRITE;
/*!40000 ALTER TABLE `blades` DISABLE KEYS */;

INSERT INTO `blades` (`id`, `name`, `ip`, `created_at`, `updated_at`, `zone_id`, `model_id`)
VALUES
	(1,'bc1-oduyu','10.80.87.32','2013-08-20 18:09:17','2013-08-25 11:53:05',7,5),
	(2,'bc2-oduyu','10.8.2.13','2013-08-25 11:28:36','2013-08-25 11:28:36',7,5);

/*!40000 ALTER TABLE `blades` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы commutators
# ------------------------------------------------------------

DROP TABLE IF EXISTS `commutators`;

CREATE TABLE `commutators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `serial_num` varchar(255) DEFAULT NULL,
  `inventory_num` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `commutators` WRITE;
/*!40000 ALTER TABLE `commutators` DISABLE KEYS */;

INSERT INTO `commutators` (`id`, `name`, `model_id`, `serial_num`, `inventory_num`, `created_at`, `updated_at`)
VALUES
	(1,'asdasd',2,'asdasd','sfddf','2013-09-15 13:41:22','2013-09-15 13:41:22');

/*!40000 ALTER TABLE `commutators` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы dcat_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dcat_items`;

CREATE TABLE `dcat_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `dcat_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dcat_items` WRITE;
/*!40000 ALTER TABLE `dcat_items` DISABLE KEYS */;

INSERT INTO `dcat_items` (`id`, `name`, `created_at`, `updated_at`, `dcat_id`)
VALUES
	(1,'Windows Server 2008 R2','2013-08-21 17:50:43','2013-08-21 18:45:42',2),
	(2,'Esxi 5.0.0','2013-08-21 17:54:56','2013-08-21 18:44:54',2),
	(3,'Windows Server 2003','2013-08-21 17:56:54','2013-08-21 18:44:05',2),
	(4,'IBM x3650','2013-08-21 18:05:21','2013-08-21 18:35:41',3),
	(5,'BC Mod1','2013-08-21 18:46:21','2013-09-19 17:55:07',1),
	(6,'BC Mod2','2013-08-21 18:47:00','2013-09-19 17:55:28',1),
	(10,'ts5200','2013-08-25 18:14:32','2013-08-25 18:14:32',4),
	(11,'планшет','2013-09-08 16:22:30','2013-09-08 16:22:30',5),
	(12,'ноутбук','2013-09-08 16:22:38','2013-09-08 16:22:38',5),
	(13,'коммуникатор','2013-09-08 16:22:47','2013-09-08 16:22:47',5),
	(14,'личный','2013-09-08 16:23:15','2013-09-08 16:23:15',6),
	(15,'рабочий','2013-09-08 16:23:23','2013-09-08 16:23:23',6),
	(16,'в работе','2013-09-15 11:41:37','2013-09-15 11:41:37',7),
	(17,'выключено','2013-09-15 11:41:56','2013-09-15 11:41:56',7),
	(19,'не в эксплуатации','2013-09-15 16:19:21','2013-09-15 16:19:21',7),
	(20,'HS22','2013-09-19 17:56:28','2013-09-19 17:56:28',8),
	(21,'HS23','2013-09-19 17:56:37','2013-09-19 17:56:37',8),
	(22,'физический','2013-09-23 14:57:39','2013-09-23 14:57:39',9),
	(23,'виртуальный','2013-09-23 14:57:50','2013-09-23 14:57:50',9),
	(24,'x86','2013-09-26 17:34:48','2013-09-26 17:34:48',10),
	(25,'x64','2013-09-26 17:34:59','2013-09-26 17:34:59',10);

/*!40000 ALTER TABLE `dcat_items` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы dcats
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dcats`;

CREATE TABLE `dcats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dcats` WRITE;
/*!40000 ALTER TABLE `dcats` DISABLE KEYS */;

INSERT INTO `dcats` (`id`, `name`, `alias`, `created_at`, `updated_at`)
VALUES
	(1,'Модель Блэйд центра','blade','2013-08-21 15:59:15','2013-08-21 15:59:15'),
	(2,'Операционная система','os','2013-08-21 16:00:00','2013-08-21 16:00:00'),
	(3,'Модель сервера','server_model','2013-08-21 18:00:40','2013-08-21 18:00:40'),
	(4,'Ленточная библиотека','tape_library','2013-08-25 18:11:57','2013-08-25 18:11:57'),
	(5,'Тип устройства пользователя','devise_type','2013-09-08 16:20:59','2013-09-08 16:20:59'),
	(6,'Принадлежность устройства','devise_own','2013-09-08 16:21:59','2013-09-08 16:51:49'),
	(7,'Состояние сервера','condition','2013-09-15 11:41:06','2013-09-15 11:41:06'),
	(8,'Модель Блэйд лезвия.','blade_part','2013-09-19 17:56:05','2013-09-22 10:21:37'),
	(9,'Тип сервера','server_type','2013-09-23 14:57:00','2013-09-23 14:57:00'),
	(10,'разрядность ОС','os_bit','2013-09-26 17:34:14','2013-09-26 17:34:14');

/*!40000 ALTER TABLE `dcats` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'Сервер','2013-01-23 17:39:54','2013-01-23 17:39:54'),
	(2,'Blade center','2013-01-23 17:53:29','2013-01-23 17:53:29'),
	(3,'Ленточная библиотека','2013-01-23 17:53:53','2013-01-23 17:53:53');

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы groups_sub_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups_sub_groups`;

CREATE TABLE `groups_sub_groups` (
  `group_id` int(11) DEFAULT NULL,
  `sub_group_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы histories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `histories`;

CREATE TABLE `histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `histories` WRITE;
/*!40000 ALTER TABLE `histories` DISABLE KEYS */;

INSERT INTO `histories` (`id`, `user_id`, `message`, `created_at`, `updated_at`)
VALUES
	(1,1,'just test','2013-01-13 17:27:03','2013-01-13 17:27:03');

/*!40000 ALTER TABLE `histories` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `items`;

CREATE TABLE `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` text,
  `group_id` int(11) DEFAULT NULL,
  `rackspace_id` int(11) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `mac` varchar(50) DEFAULT NULL,
  `serial_num` varchar(50) DEFAULT NULL,
  `inventory_num` varchar(50) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `is_virtual` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parts`;

CREATE TABLE `parts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  `blade_id` int(11) DEFAULT NULL,
  `server_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `parts` WRITE;
/*!40000 ALTER TABLE `parts` DISABLE KEYS */;

INSERT INTO `parts` (`id`, `model_id`, `num`, `blade_id`, `server_id`, `created_at`, `updated_at`)
VALUES
	(1,20,1,1,2,'2013-09-22 09:24:59','2013-09-22 09:24:59'),
	(2,20,2,1,NULL,'2013-09-22 09:27:34','2013-09-22 09:27:34'),
	(5,21,3,2,4,'2013-09-22 09:43:11','2013-09-22 09:43:11');

/*!40000 ALTER TABLE `parts` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы rackshelves
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rackshelves`;

CREATE TABLE `rackshelves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `rackshelves` WRITE;
/*!40000 ALTER TABLE `rackshelves` DISABLE KEYS */;

INSERT INTO `rackshelves` (`id`, `name`, `area_id`, `created_at`, `updated_at`)
VALUES
	(1,'1-1',2,'2013-01-13 12:02:01','2013-01-13 12:02:01'),
	(2,'1-3',3,'2013-01-18 17:19:32','2013-01-18 17:19:32');

/*!40000 ALTER TABLE `rackshelves` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы rackspaces
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rackspaces`;

CREATE TABLE `rackspaces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `rackspaces` WRITE;
/*!40000 ALTER TABLE `rackspaces` DISABLE KEYS */;

INSERT INTO `rackspaces` (`id`, `name`, `area_id`, `created_at`, `updated_at`)
VALUES
	(1,'1-1',1,'2013-08-25 17:33:42','2013-08-25 17:46:47'),
	(3,'3-1',2,'2013-08-25 17:37:59','2013-08-25 17:37:59'),
	(4,'3-2',2,'2013-08-25 17:38:07','2013-08-25 17:38:07'),
	(5,'after delete',4,'2013-08-25 17:52:41','2013-08-25 17:52:41'),
	(6,'ssssss',NULL,'2013-08-25 17:55:17','2013-08-25 17:55:17'),
	(7,'1-3',1,'2013-10-07 15:41:03','2013-10-07 15:57:29'),
	(9,'1-4',1,'2013-10-07 15:57:02','2013-10-07 15:57:02');

/*!40000 ALTER TABLE `rackspaces` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы schema_migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `schema_migrations`;

CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;

INSERT INTO `schema_migrations` (`version`)
VALUES
	('20121223161339'),
	('20121226152356'),
	('20121226163118'),
	('20130113172047'),
	('20130115151120'),
	('20130123173427'),
	('20130127170934'),
	('20130127171536'),
	('20130203140845'),
	('20130820173952'),
	('20130821155109'),
	('20130821160628'),
	('20130821164623'),
	('20130822184351'),
	('20130822192216'),
	('20130825094516'),
	('20130825125711'),
	('20130825180057'),
	('20130827164435'),
	('20130827170450'),
	('20130915133947'),
	('20130919172852'),
	('20130922090532'),
	('20130923150622'),
	('20130923172951'),
	('20130926173548'),
	('20131008174005');

/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы servers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `servers`;

CREATE TABLE `servers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `zone_id` int(11) DEFAULT NULL,
  `rackspace_id` int(11) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `mac` varchar(255) DEFAULT NULL,
  `serial_num` varchar(255) DEFAULT NULL,
  `inventory_num` varchar(255) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `model_id` varchar(255) DEFAULT NULL,
  `model_details` varchar(255) DEFAULT NULL,
  `os_id` varchar(255) DEFAULT NULL,
  `os_details` varchar(255) DEFAULT NULL,
  `hardware` text,
  `software` text,
  `status_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `os_bit` int(11) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `servers` WRITE;
/*!40000 ALTER TABLE `servers` DISABLE KEYS */;

INSERT INTO `servers` (`id`, `name`, `description`, `zone_id`, `rackspace_id`, `ip`, `mac`, `serial_num`, `inventory_num`, `rating`, `model_id`, `model_details`, `os_id`, `os_details`, `hardware`, `software`, `status_id`, `created_at`, `updated_at`, `type_id`, `os_bit`, `date_start`)
VALUES
	(1,'bc1-oduyu','Сервер \r\nрезервного копирования',7,NULL,'10.80.1.1','aaksdja',NULL,NULL,1,NULL,NULL,'3','SP1','Memory: 20Gb\r\nCPU: Core i5','Описание программного обеспечениz',16,'2013-08-29 17:10:34','2013-10-08 18:21:40',23,24,'2013-10-16'),
	(2,'1c-oduyu-new','askdjka',7,3,'10.80.1.13','42323423','dsfsd234fds','sdf2423',3,'4','','2','fdsf','sfsdfsdf','hgfhsa',16,'2013-09-14 16:29:58','2013-10-08 18:25:16',22,24,'2013-10-17'),
	(3,'vc1-oduyu','VMWare Sphere Center',7,1,'10.80.1.44','fdfdsadasd','gffdgsd','sdfsdf',3,'4','32423','1','x64','Memory : 8Gb','VSphere 5.0',16,'2013-09-15 11:26:14','2013-10-09 10:26:16',22,24,'2013-10-07'),
	(4,'bav1-oduyu','lorem ipsum',7,NULL,'10.80.1.13','fdfdsadasd',NULL,NULL,2,NULL,NULL,'3','x64 SP1','CPU 2 XEON','Server 2008',16,'2013-09-15 15:27:35','2013-09-24 17:18:31',23,NULL,NULL);

/*!40000 ALTER TABLE `servers` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы sub_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sub_groups`;

CREATE TABLE `sub_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sub_groups` WRITE;
/*!40000 ALTER TABLE `sub_groups` DISABLE KEYS */;

INSERT INTO `sub_groups` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'Процессор','2013-01-27 17:12:55','2013-01-27 17:12:55'),
	(2,'HDD','2013-01-27 17:13:07','2013-01-27 17:13:07'),
	(3,'Оперативная память','2013-01-27 17:13:23','2013-01-27 17:13:23'),
	(4,'Чипсет','2013-01-27 17:43:22','2013-01-27 17:43:22'),
	(5,'Network adapter','2013-01-27 17:43:45','2013-01-27 17:43:45'),
	(6,'Optical adapter','2013-01-27 17:44:03','2013-01-27 17:44:03'),
	(7,'ОС','2013-01-27 17:44:19','2013-01-27 17:44:19'),
	(8,'SQL','2013-01-27 17:44:35','2013-01-27 17:44:35');

/*!40000 ALTER TABLE `sub_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы tape_libraries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tape_libraries`;

CREATE TABLE `tape_libraries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `rackspace_id` int(11) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tape_libraries` WRITE;
/*!40000 ALTER TABLE `tape_libraries` DISABLE KEYS */;

INSERT INTO `tape_libraries` (`id`, `name`, `ip`, `zone_id`, `rackspace_id`, `model_id`, `created_at`, `updated_at`)
VALUES
	(2,'ts3200-oduyu','10.80.1.11',7,3,10,'2013-09-03 17:06:39','2013-09-17 17:48:16');

/*!40000 ALTER TABLE `tape_libraries` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT '0',
  `password_digest` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `firstname` varchar(30) DEFAULT NULL,
  `lastname` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `is_admin`, `password_digest`, `created_at`, `updated_at`, `firstname`, `lastname`)
VALUES
	(1,'max',0,'$2a$10$Db4fdPbWvPXIVCLX4gd4Veo9QqOf3w5KkNqrJq5Xk.1oKaeWEhzc6','2013-01-12 08:56:16','2013-01-15 15:20:51','Максим','Густяков'),
	(2,'stepan',0,'$2a$10$ZABcYYBErNh5DPLaAUOfKOM4sq6kjZE5waVAHhwmvJGos68toNFUC','2013-01-12 16:46:18','2013-01-12 16:46:18',NULL,NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы wifis
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wifis`;

CREATE TABLE `wifis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `mac` varchar(255) DEFAULT NULL,
  `devise_type_id` int(11) DEFAULT NULL,
  `devise_own_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `wifis` WRITE;
/*!40000 ALTER TABLE `wifis` DISABLE KEYS */;

INSERT INTO `wifis` (`id`, `username`, `model`, `mac`, `devise_type_id`, `devise_own_id`, `created_at`, `updated_at`)
VALUES
	(2,'Петров Петр Петрович','Iphone 4s','21:21:4F:43:53',12,14,'2013-09-12 17:53:33','2013-09-18 18:13:09'),
	(3,'Stepan','HTC OneX','1c:b0:94:c9:2d:3e',11,15,'2013-09-22 15:08:36','2013-09-22 15:08:36');

/*!40000 ALTER TABLE `wifis` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы zones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `zones`;

CREATE TABLE `zones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `zones` WRITE;
/*!40000 ALTER TABLE `zones` DISABLE KEYS */;

INSERT INTO `zones` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'Астраханское РДУ','2013-08-22 18:45:56','2013-08-22 18:45:56'),
	(2,'Волгоградское РДУ','2013-08-22 18:46:58','2013-08-22 18:46:58'),
	(3,'Дагестанское РДУ','2013-08-22 18:47:08','2013-08-22 18:47:08'),
	(4,'Кубанское РДУ','2013-08-22 18:49:18','2013-08-22 18:49:18'),
	(5,'Ростовское РДУ','2013-08-22 18:49:28','2013-08-22 18:49:28'),
	(6,'Северокавказское РДУ','2013-08-22 18:49:40','2013-08-22 18:49:40'),
	(7,'ОДУ ЮГА','2013-08-22 18:49:51','2013-08-22 18:49:51');

/*!40000 ALTER TABLE `zones` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
