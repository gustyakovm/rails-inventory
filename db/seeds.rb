#!/bin/env ruby
# encoding: utf-8

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' },

#   Mayor.create(name: 'Emanuel', city: cities.first)

# encoding: UTF-8

ind = ReplacePart.create([
	
	{ network_name: "SMIS2-ODUYU", detail_type: "HDD", date_sending: "2013-12-03", date_getting: "2013-12-02", descr_sending: "P/n: 42D0681 FRU: 42D0678 146Gb 15k rpm 6Gb SAS", descr_getting: "P/n: 90Y8930 FRU: 90Y8927 146Gb 15K rpm" }, { network_name: "SMSGATE-ODUYU", detail_type: "HDD", date_sending: "2013-12-03", date_getting: "2013-12-02", descr_sending: "P/N: 26K5837 FRU: 39R7340 73.4 Gb 10k rmp SAS", descr_getting: "P/N: 44W2238 FRU: 44W2235 300Gb 15k rpm 6Gb SAS" }, { network_name: "FINIST-ODUYU", detail_type: "HDD", date_sending: "2013-12-03", date_getting: "2013-12-02", descr_sending: "P/N: 26K5842 FRU: 39R7350 146Gb 15K rpm SAS", descr_getting: "P/N: 44W2238 FRU: 44W2235 300Gb 15K rpm 6Gb Sas" }, { network_name: "CKP702-oduyu", detail_type: "hdd", date_sending: "2013-12-03", date_getting: "2013-11-27", descr_sending: "FRU:39R7340 P/N:76K5837", descr_getting: "" }, { network_name: "CKP702-ODUYU", detail_type: "HDD", date_sending: "", date_getting: "2013-11-08", descr_sending: "HDD IBM FRU: 42D0678 IBM P/N: 42D0681 146Gb 15K RPM SAS SFF", descr_getting: "HDD IBM FRU: 90Y8927 IBM P/N: 90Y8930 146Gb 15K RPM SAS SFF" }, { network_name: "DS4300-ODUYU", detail_type: "Батарейка", date_sending: "2013-10-22", date_getting: "2013-11-14", descr_sending: "Батареи 2 шт: P/N 3204 4V - 3.2 Ah", descr_getting: "Батареи 2 шт: P/N 3204 4V - 3.2 Ah" }, { network_name: "DS74700-203", detail_type: "HDD", date_sending: "2013-10-22", date_getting: "2013-10-21", descr_sending: "Жесткий диск IBM FRU: 42D0417 IBM P/N: 42D0413 300Gb 15krpm 4Gb Fibre", descr_getting: "Жесткий диск IBM FRU: 42D0417 IBM P/N: 42D0413 300Gb 15krpm" }, { network_name: "CBMDB1-ODUYU", detail_type: "Батарейка", date_sending: "2013-10-22", date_getting: "2013-09-30", descr_sending: "SN: SP20426181 FRU P/N: 43W4342", descr_getting: "SN: 46C9040" }, { network_name: "VH57-ODUYU", detail_type: "HDD", date_sending: "2013-09-13", date_getting: "2013-09-23", descr_sending: "IBM FRU: 42D0638 IBM P/N: 42D0641 300Gb 10K rpm 6Gb SAS", descr_getting: "IBM FRU: 90Y8878 IBM P/N: 90Y8881 300Gb 10K rpm 6Gb SAS" }, { network_name: "NO NAME", detail_type: "лезвие Blade Center", date_sending: "2013-09-13", date_getting: "2013-09-23", descr_sending: "HS 22 MT 7870 S/N 06CCK85 PRODUCT ID 7870UGR", descr_getting: "HS 22 MT 7870 S/N 06CCK85 PRODUCT ID 7870UGR" }, { network_name: "EX4-ODUYU", detail_type: "HDD", date_sending: "", date_getting: "2013-09-03", descr_sending: "IBM FRU: 42D0638 IBM P/N: 42D0641 300Gb 10k rpm 6Gb SAS", descr_getting: "IBM FRU: 90Y8878 IBM P/N: 90Y8881 300Gb 10K rpm 6Gb SAS" }, { network_name: "ARCHM1-ODUYU", detail_type: "Батарейка", date_sending: "2013-07-10", date_getting: "2013-07-12", descr_sending: "

FRU P/N: 43W4342
SN: SP03927150
", descr_getting: "FRU P/N: 46C9040
SN: SP25123931" }, { network_name: "CSPA22-ODUYU", detail_type: "Батарейка", date_sending: "2013-07-05", date_getting: "2013-07-12", descr_sending: "SN: SP04727679
FRU: 43W4342

В ЦСПА не было батарейки,
батарейку нашли в ящике", descr_getting: "" }, { network_name: "CSPA12-ODUYU", detail_type: "Батарейка", date_sending: "2013-07-05", date_getting: "2013-07-12", descr_sending: "FRU: 43W4342
PN:44E8761
SN: SP20426443

В ЦСПА не было батарейки,
батарейку нашли в ящике", descr_getting: "" }, { network_name: "OIK702-ODUYU", detail_type: "HDD", date_sending: "2013-07-05", date_getting: "2013-07-08", descr_sending: "

FRU       42D0638
P/N        42D0641
", descr_getting: "FRU       90Y8878
P/N        90Y8881" }, { network_name: "ARCHM2-ODUYU", detail_type: "Батарейка", date_sending: "2013-07-05", date_getting: "2013-06-17", descr_sending: "", descr_getting: "SN: SP24907874
FRU: 46C9040" }, { network_name: "1C-ODUYU-NEW", detail_type: "Батарейка", date_sending: "", date_getting: "2013-06-17", descr_sending: "", descr_getting: "SN: SP24907874
FRU: 46C9040" }, { network_name: "ECHO2-ODUYU", detail_type: "плата OLHA", date_sending: "2013-05-23", date_getting: "2013-03-13", descr_sending: "OLHA 9P/C/ISDN8", descr_getting: "" }, { network_name: "YU-ODU-CSM-1", detail_type: "HDD", date_sending: "2013-06-05", date_getting: "2013-05-16", descr_sending: "IBM FRU: 43X0825
IBM P/N: 42C0248
146 Gb
10k rpm
SAS", descr_getting: "получен но не записан" }, { network_name: "EX4-ODUYU", detail_type: "Батарейка", date_sending: "", date_getting: "2013-05-21", descr_sending: "

IBM FRU P/N: 43W4342
SN: SP04729152
", descr_getting: "FRU: 46C9040" }, { network_name: "MQ2-ODUYU", detail_type: "HDD", date_sending: "", date_getting: "2013-05-23", descr_sending: "

IBM FRU: 39R7350
IBM P/N: 26K5842
146 Gb
15krpm
SAS
", descr_getting: "

IBM FRU: 39R7350
IBM P/N: 26K5842
146 Gb
15krpm
SAS
" }, { network_name: "FMON-A", detail_type: "HDD", date_sending: "", date_getting: "2013-05-23", descr_sending: "

IBM FRU: 39R7342
IBM P/N: 26K5838
146.8 Gb
10krpm
SAS
", descr_getting: "

IBM FRU: 39R7350
IBM P/N: 26K5842
146.8 Gb
15krpm
SAS
" }, { network_name: "CSPA21-ODUYU", detail_type: "HDD", date_sending: "2013-04-08", date_getting: "2013-04-12", descr_sending: "IBM FRU: 42D0638
IBM P/N: 42D0641", descr_getting: "IBM FRU: 90Y8878
IBM P/N: 90Y8881
300Gb
10K rpm
6Gb SAS" }, { network_name: "CSPA21-ODUYU", detail_type: "HDD", date_sending: "2013-04-08", date_getting: "2013-04-12", descr_sending: "IBM FRU: 42D0638
IBM P/N: 42D0641", descr_getting: "IBM FRU: 90Y8878
IBM P/N: 90Y8881
300Gb
10K rpm
6Gb SAS" }, { network_name: "BS2-ODUYU", detail_type: "Блок питания ленточной библиотеки", date_sending: "2013-04-23", date_getting: "2013-03-22", descr_sending: "FRU:59P6709 EC = H14812 60-460S065", descr_getting: "FRU: 59P6709 Model #RAS-2662P 60-460-065" }, { network_name: "CSPA1-203", detail_type: "Батарейка", date_sending: "2013-04-23", date_getting: "2013-03-22", descr_sending: "FRU: 43W4342", descr_getting: "FRU: 46C9040" }, { network_name: "VCS2-ODUYU", detail_type: "Батарейка", date_sending: "2013-04-23", date_getting: "2013-03-22", descr_sending: "FRU: 43W4342", descr_getting: "FRU: 46C9040" }, { network_name: "oik7t-oduyu", detail_type: "Батарейка", date_sending: "2013-03-12", date_getting: "2013-02-27", descr_sending: "IBM FRU P/N 43W4342", descr_getting: "" }, { network_name: "не зафиксировано", detail_type: "HDD", date_sending: "2013-03-12", date_getting: "2013-02-27", descr_sending: "IBM FRU: 59Y5336 IBM P/N: 59Y5338 600Gb 15k rpm 4Gb Fibre", descr_getting: "" }, 

	])